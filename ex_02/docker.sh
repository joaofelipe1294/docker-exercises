#!/bin/bash

# RUN cria um novo container

docker run -d --name first_container \
  -v /Users/joaolopes/Documents/das-ufpr/devops/docker-exercises/ex_02/pages:/usr/share/nginx/html \
  -p 80:80 nginx:latest \

docker run -d --name second_container \
  -v /Users/joaolopes/Documents/das-ufpr/devops/docker-exercises/ex_02/pages:/usr/share/nginx/html \
  -p 8080:80 nginx:latest

# start apenas inicia
docker start first_container && docker start second_container
